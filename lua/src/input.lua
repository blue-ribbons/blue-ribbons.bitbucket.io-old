--// Player input controller bound to pager class.
--// Represents an abstraction layer between player input and various available actions.

local signal = require("libs/hump/signal")
local class = require "libs/hump/class"
local timer = require "libs/hump/timer"
local mceil = math.ceil

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc

input = class {}

function input:init()
	
--	self.font_large = br.fonts["large"]
	self.state = 0	--// 0 - free, 1 - busy
	self.color_single = { 0, 0, 0, 0 }
	self.color_multiple_bg = { 255, 255, 255, 25 }
	self.color_multiple_fg = { 255, 255, 255, 255 }
	self.color_active = { br.activeColor[1], br.activeColor[2], br.activeColor[3], 35 }
	self.type = 0	--// 0 - single, 1 - multiple
	self.buttons = {}
	
end

function input:update(dt)
--	gui.update(dt)
	if #self.buttons > 1 then	gui.get(self.buttons[self.active]).bgColor = self.color_active	end
end

function input:draw()
--	gui.draw("input")
	if self.state == 1 and self.type == 0 then
		love.graphics.setColor(self.color_active[1], self.color_active[2], self.color_active[3], 255)
	--	love.graphics.setFont(self.font_large)
	--	love.graphics.print("<...>", cx + 208 * sc, self.cursorY, 0, sc, sc)
		love.graphics.print("<...>", sW / 2 - 12 * sc, sH / 2 + 256 * sc, 0, sc, sc)
	end
end

function input:keypressed(key, unicode, handle)
	if handle.state == 1 then
		if handle.type == 0 then
			TEsound.play(br.sfx[1], "click", 0.6) 
			table.remove(handle.buttons, 1) 
			gui.remove("single") 
			handle.state = 0
		elseif handle.type == 1 then
			local switch = {
				["up"] = function()	TEsound.play(br.sfx[2], "switch", 0.6)	handle.onKeyUp()	end,
				["down"] = function()	TEsound.play(br.sfx[2], "switch", 0.6)	handle.onKeyDown()	end,
				["w"] = function()	TEsound.play(br.sfx[2], "switch", 0.6)	handle.onKeyUp()	end,
				["s"] = function()	TEsound.play(br.sfx[2], "switch", 0.6)	handle.onKeyDown()	end,
				["return"] = function()	handle.action(handle.active)	end,
				[" "] = function()	handle.action(handle.active)	end,
				["e"] = function()	handle.action(handle.active)	end,
				["x"] = function()	handle.action(handle.active)	end,
				["1"]  = function()	handle.action(1)				end,
				["2"]  = function()	handle.action(2)				end,
				["3"]  = function()	handle.action(3)	end,
				["4"]  = function()	handle.action(4)	end,
				["kp1"]  = function()	handle.action(1)			end,
				["kp2"]  = function()	handle.action(2)	end,
				["kp3"]  = function()	handle.action(3)	end,
				["kp4"]  = function()	handle.action(4)	end,
			}
			if switch[key] then switch[key]()	end			
		end
	end
end

function input:joystickpressed(joystick, button, handle)
	if handle.state == 1 then
		if handle.type == 0 then
			TEsound.play(br.sfx[1], "click", 0.6) 
			table.remove(handle.buttons, 1) 
			gui.remove("single") 
			handle.state = 0
		elseif handle.type == 1 then
			local switch = {
				[1] = function()	handle.action(handle.active)	end,
				[2] = function()	handle.action(handle.active)	end,
				[3] = function()	handle.action(handle.active)	end,
				[4] = function()	handle.action(handle.active)	end,
				[5] = function()	handle.onKeyUp()	end,
				[6] = function()	handle.onKeyDown()	end,
				[7] = function()	handle.onKeyUp()	end,
				[8] = function()	handle.onKeyDown()	end,
			}
			if switch[button] then switch[button]()	end	
		end
	end
end

function input:single()
	self.type = 0
	gui.newButton("single", _, 0, 0, sW, sH, _, "input")				--// button origins: ( cs + 176 * sc, 132 * sc, 800 * sc, 600 * sc )
	gui.get("single").fgColor, gui.get("single").bgColor = self.color_single, self.color_single
	gui.get("single").showBorder = false
	table.insert(self.buttons, "single")
	gui.get("single"):onRelease(function() TEsound.play(br.sfx[1], "click", 0.6) table.remove(self.buttons, 1) gui.remove("single") self.state = 0	end)
end

function input:multiple(m, lines)	
	self.type = 1
	self.action =  function(id)
		if id <= #self.buttons then
			TEsound.play(br.sfx[1], "click", 0.6)
			for i = 1, #self.buttons, 1 do
				table.remove(self.buttons, i)
				gui.remove("multiple-"..i)
			end
			self.active, self.actions = nil, nil 
			self.onKeyUp, self.onKeyDown = nil, nil
--			about.session = {}
			about.session.cslot.script = m[id]["name"]
			about.session.cslot.line = 1
			GS.switch(require("states/gamescreen"))
		end
	end
	
	self.onKeyUp = function()
		TEsound.play(br.sfx[2], "switch", 0.6)
		gui.get(self.buttons[self.active]).bgColor = self.color_multiple_bg		
		if self.active - 1 >= 1 then
			self.active = self.active - 1	
		else
			self.active = #self.buttons		
		end	
	end
	
	self.onKeyDown = function()
		TEsound.play(br.sfx[2], "switch", 0.6)
		gui.get(self.buttons[self.active]).bgColor = self.color_multiple_bg		
		if self.active + 1 <= #self.buttons then			
			self.active = self.active + 1
		else
			self.active = 1			
		end		
	end
	local correction = { ["en"] = 50, ["ru"] = 70 }
	local height, add_delta = {}, {}
	for i = 1, #m, 1 do
		local l = mceil((#m[i]["text"] + 3)/correction[about.settings.lang])
		if l > 1 then
			table.insert(height, (64 * l) * sc)
			add_delta[i + 1] =  (32 * l) * sc
		else
			table.insert(height, 64 * sc)
		end
	end
	
	lines = lines + 2
	for i = 1, #m, 1 do
		if add_delta[i] then
			gui.newButton("multiple-"..i, i..". "..m[i]["text"], cx + 206 * sc, (800/12 * (lines + 1*i) + add_delta[i] + 24 * i) * sc, 740 * sc, height[i], _, "input")
		else
			gui.newButton("multiple-"..i, i..". "..m[i]["text"], cx + 206 * sc, (800/12 * (lines + 1*i) + 24 * i) * sc, 740 * sc, height[i], _, "input")
		end
		gui.get("multiple-"..i).fgColor, gui.get("multiple-"..i).bgColor = self.color_multiple_fg, self.color_multiple_bg	
		gui.get("multiple-"..i).showBorder, gui.get("multiple-"..i).uses_printf = false, true
		gui.get("multiple-"..i):onRelease(function() self.action(i) end)
		table.insert(self.buttons, "multiple-"..i)
	end
	self.active = 1
	

end

function input:destroy()
	for i = 1, #self.buttons, 1 do
		gui.remove(self.buttons[i])
	end
	self.buttons = {}	
end