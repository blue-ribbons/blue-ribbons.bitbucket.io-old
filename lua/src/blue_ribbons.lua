local br = {}	-- a table of objects I'd rather load once than every time the gamestate changes
local mpow, mfloor, sbyte = math.pow, math.floor, string.byte

br.gpColor = { fg = { 156, 156, 156 }, bg = { 181, 181, 181, 75 } }
br.uiColor = { fg = {255, 255, 255 }, bg = {255, 255, 255, 50 } }
br.activeColor = { 0, 191, 255, 75 }

br.sfx = {
--[[	[1] = love.sound.newSoundData("sound/sfx/fs1.ogg"),
	[2] = love.sound.newSoundData("sound/sfx/fs2.ogg"),
	[3] = love.sound.newSoundData("sound/sfx/se085.ogg"),
	[4] = love.sound.newSoundData("sound/sfx/fs3.ogg"),
	[6] = love.sound.newSoundData("sound/sfx/se077.ogg"),	]]
	[1] = "sound/sfx/fs1.ogg",
	[2] = "sound/sfx/fs2.ogg",
	[3] = "sound/sfx/se085.ogg",
	[4] = "sound/sfx/fs3.ogg",
	[5] = "sound/sfx/se084.ogg",
	[6] = "sound/sfx/se077.ogg",
}

br.math_round = function( roundIn , roundDig )
	
	if not roundDig then roundDig = 2 end
	
	local mul = mpow( 10, roundDig )
    return ( mfloor( ( roundIn * mul ) + 0.5 ) / mul )

end

br.sW, br.sH = love.graphics.getWidth(), love.graphics.getHeight()
br.sc = br.math_round(br.sH / 720, 3)

if br.sc ~= 1 then
	br.cx, br.cy = br.sW / 2 - 475 * br.sc, 0
	br.xc, br.yc = br.sW / 2 + 475 * br.sc, br.sH
else
	br.cx, br.cy, br.xc, br.yc = 0, 0, 950, 720
end
--[[
br.za = {		--// zoom areas
	["top-left"] = { x = 192 * br.sc, y = 144 * br.sc },
	["top-center"] = { x = 576 * br.sc, y = 144 * br.sc },
	["top-right"] = { x = 960 * br.sc, y = 144 * br.sc },
	["center-left"] = { x = 192 * br.sc , y = 432 * br.sc },
	["center-center"] = { x = 576 * br.sc , y = 432 * br.sc },
	["center-right"] = { x = 960 * br.sc, y = 432 * br.sc },
	["bottom-left"] = { x = 192 * br.sc, y = 720 * br.sc },
	["bottom-center"] = { x = 576 * br.sc , y = 720 * br.sc },
	["bottom-right"] = { x = 960 * br.sc, y = 720 * br.sc },
}	]]

br.za = {			--// new coords were manifested because page origin changed from top-left to center
	["top-left"] = { x = 384 * br.sc, y = 288 * br.sc },
--	["top-center"] = { x = 0, y = -288 * br.sc },
	["top-center"] = { x = 0, y = 288 * br.sc },
	["top-right"] = { x = -384 * br.sc, y = 288 * br.sc },
	["center-left"] = { x = 384 * br.sc, y = 0 },
	["center-center"] = { x = 0, y = 0 },
	["center-right"] = { x = -384 * br.sc , y = 0 },
	["bottom-left"] = { x = 384 * br.sc, y = -288 * br.sc },
--	["bottom-center"] = { x = 0, y = 288 * br.sc },
	["bottom-center"] = { x = 0, y = -288 * br.sc },
	["bottom-right"] = { x = -384 * br.sc, y = -288 * br.sc },
}

br.fonts = {
	["regular"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 32 * br.sc),
	["large"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 64 * br.sc),
}

return br