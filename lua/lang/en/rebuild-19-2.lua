local src = {}
src[1] = '"___________"\nI know whatʼs right.'
src[2] = 'The thing that keeps me alive.\nI canʼt turn my back on the thing that has kept me alive.'
src[3] = 'I canʼt let my conviction waver.\nFor the sake of everyone who could not be saved, I canʼt let any more people share in their fate.'
src[4] = 'Thereʼs no need to talk about the obvious outcome.\nNozaki Umetarou will turn his mind to steel, and become a superhero.'
return src