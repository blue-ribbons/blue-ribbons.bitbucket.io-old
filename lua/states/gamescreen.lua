require "src/pager"
local timer = require("libs/hump/timer")
local gamescreen = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function gamescreen:init()

	
end

function gamescreen:enter(state, rewind)

	about.scene = "gamescreen"
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)

	
	self.joystickBlocked = false
	
	--// either resume pager from image, restore one from saved script and line pointers or make a completely new one
	if about.session.cslot.script then
		if about.session.cslot.line then
			self.pager = pager()
		--	self.pager.current_line = about.session.cslot.line
		--	self.pager:open(about.session.cslot.script, about.session.cslot.line - 1)
			print("rewind: "..tostring(rewind))
			if rewind then self.pager:rewind(0, about.session.cslot.script, about.session.cslot.line)
			else		
				self.pager:open(about.session.cslot.script, about.session.cslot.line)
				self.pager:read()
			end
		else
			self.pager = pager()
			self.pager:open(about.session.cslot.script)
			self.pager:read()
		end
	else
	end
	
end

function gamescreen:leave()
	self.timer:clear()
end

function gamescreen:resume()
	self.pager:rewind()
end

function gamescreen:update(dt)
	
	self.pager:update(dt)
	
end

function gamescreen:draw()
	
	
	self.pager:draw()
	
end

function gamescreen:leave()

end

function gamescreen:keypressed(key, unicode)
	
end

function gamescreen:joystickpressed(joystick, button)
	
end

return gamescreen