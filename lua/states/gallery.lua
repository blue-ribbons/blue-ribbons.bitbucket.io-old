local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local gallery = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function gallery:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.sfx = br.sfx
	self.timer = timer.new()
	
end

function gallery:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "gallery"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = lang.empty },
			[2] = { scene = lang.empty },
			[3] = { scene = lang.empty },
			[4] = { scene = lang.empty },
			[5] = { scene = lang.empty },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
		},
	}				
	if love.filesystem.exists("battery_save") then
		local data = love.filesystem.read("battery_save")
		savefile = json.decode(data)
	end
	
	local labels = {
		[1] = {
			[1] = "Doushio",		--// 1/25
			[2] = "graphics/static/i25.png",
		},
		[2] = {
			[1] = "Pantsu Shot",		--// 1/28
			[2] = "graphics/static/i28.png",
		},
		[3] = {
			[1] = "Goodnight",		--// 2/4-1
			[2] = "graphics/static/i4-1.png",
		},
		[4] = {
			[1] = "Shinu",		--// 2/4-2
			[2] = "graphics/static/i4-2.png",
		},
		[5] = {
			[1] = "Ahegao 1",		--//	2/9
			[2] = "graphics/static/i9.png",
		},
		[6] = {
			[1] = "Green Route",		--// 2/18-2
			[2] = "graphics/static/i18-2.png",
		},
		[7] = {
			[1] = "Purple Route",		--// 2/18-3
			[2] = "graphics/static/i18-3.png",
		},
		[8] = {
			[1] = "Bread Exploded",		--// 2/26
			[2] = "graphics/static/i26.png",
		},
		[9] = {
			[1] = "Nisemono Lovers",		--// 1/30
			[2] = "graphics/static/i30.png",
		},
		[10] = {
			[1] = "Ahegao 2",		--// 2 /37
			[2] = "graphics/static/i37.png",
		},
	}
	
	self.menu = { buttons = {}, active = 1 }
	
	self.src = {
		[1] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/25",
			[4] = "graphics/static/ilocked.png",
		},
		[2] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/28",
			[4] = "graphics/static/ilocked.png",
		},
		[3] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/4-1",
			[4] = "graphics/static/ilocked.png",
		},
		[4] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/4-2",
			[4] = "graphics/static/ilocked.png",
		},
		[5] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/9",
			[4] = "graphics/static/ilocked.png",
		},
		[6] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/18-2",
			[4] = "graphics/static/ilocked.png",
		},
		[7] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/18-3",
			[4] = "graphics/static/ilocked.png",
		},
		[8] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/26",
			[4] = "graphics/static/ilocked.png",
		},
		[9] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/30",
			[4] = "graphics/static/ilocked.png",
		},
		[10] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/37",
			[4] = "graphics/static/ilocked.png",
		},
	}
	
	for i = 1, #savefile.achievements.img, 1 do
		if savefile.achievements.img[i] == 1 then
			self.src[i][1] = 1
			self.src[i][2] = labels[i][1]
			self.src[i][4] = labels[i][2]
		end
	end
	
	self.action = function(id)
		if id ~= 11 then
			if self.src[id][1] == 1 then
				TEsound.play(self.sfx[1], "click", 0.6)
				GS.switch(require("states/imgviewer"), "graphics/"..self.src[id][3]..".jpg")
			else
				TEsound.play(self.sfx[1], "click", 0.6)
			end
		else
			self.quit()
		end
	end
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/mainmenu"))
	end
	
	local delta = 112
	for i = 1, 5, 1 do		
	--	gui.newButton("gallery-"..i, self.src[i][2], sW / 5  - 140 * sc, (340 + delta * i) * sc, 280 * sc, 60 * sc)
		gui.newButton("gallery-"..i, self.src[i][2], sW / 2  - 420 * sc, (delta * i) * sc, 380 * sc, 96 * sc, self.src[i][4], _, sc)
		table.insert(self.menu.buttons, "gallery-"..i)	
	end
	
	delta = 112
	for i = 6, 10, 1 do		
	--	gui.newButton("gallery-"..i, self.src[i][2], sW / 5 * 4 - 140 * sc, (340 + delta * (i - 5)) * sc, 280 * sc, 60 * sc)
		gui.newButton("gallery-"..i, self.src[i][2], sW / 2  + 420 * sc - 380 * sc, (delta * (i - 5)) * sc, 380 * sc, 96 * sc, self.src[i][4], _, sc)
		table.insert(self.menu.buttons, "gallery-"..i)	
	end
	
	
	for i = 1, #self.menu.buttons, 1 do	
		gui.get(self.menu.buttons[i]).fgColor, gui.get(self.menu.buttons[i]).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get(self.menu.buttons[i]):onRelease(function() self.action(i) end)
	end
	
	gui.newButton("gallery-11", lang.back, sW / 2 - 130 * sc, 680 * sc, 260 * sc, 60 * sc)
	gui.get("gallery-11").fgColor, gui.get("gallery-11").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("gallery-11"):setRadiusCorner(gui.get("gallery-11").h / 2)
	gui.get("gallery-11"):onRelease(self.quit)
	table.insert(self.menu.buttons, "gallery-11")
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
--	self.input = "none"
	self.joystickBlocked = false
	
end

function gallery:leave()
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("gallery-"..i)
	end
end

function gallery:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function gallery:draw()
	gui.draw()
	
	drawFPS()
	
end

function gallery:keypressed(key, unicode)
	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.quit()	end,
		["menu"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
--	self.input = key
end

function gallery:joystickpressed(joystick, button)

	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end
	
end

return gallery