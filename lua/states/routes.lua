local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local routes = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function routes:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.font = br.fonts["large"]
	self.timer = timer.new()
	self.sfx = br.sfx
	
end

function routes:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "routes"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.menu = {	buttons = {}, active = 1	}
	
	local isRouteUnlocked = false
	if love.filesystem.exists("battery_save") then
		local raw = love.filesystem.read("battery_save")
		local data = json.decode(raw)
		if data.route2 == 1 then
			isRouteUnlocked = true
		end
	end

	self.menu.actions = {
		[1]	=	function()	TEsound.play(self.sfx[1], "click", 0.6)	
							TEsound.stop("bgm")	
							about.session.cslot = {}
--							about.session.pgr_image = nil		--// destroy image if exists
							about.session.cslot.route = 1
							about.session.cslot.script = "true-1"
							about.session.cslot.shadow = {}
							GS.switch(require("states/gamescreen"))	
						end,
		[2]	=	function()	
				
				TEsound.play(self.sfx[1], "click", 0.6)		
				if isRouteUnlocked then
					TEsound.stop("bgm")	
					about.session.cslot = {}
					about.session.cslot.route = 2
					about.session.cslot.script = "rebuild-1-2"
					about.session.cslot.shadow = {}
					GS.switch(require("states/gamescreen"))
				end
				
			end,
		[3]	=	function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/mainmenu"))	end,
	}
	
	local delta = 80
	for i = 1, 2, 1 do		
		if i == 2 then
			if isRouteUnlocked then
				gui.newButton("menu-"..i, _, sW / 2 - 150 * sc, (100 + delta * (i*i)) * sc, 300 * sc, 215 * sc, "graphics/static/route_"..i..".png", _, sc)
			else
				gui.newButton("menu-"..i, _, sW / 2 - 150 * sc, (100 + delta * (i*i)) * sc, 300 * sc, 215 * sc, "graphics/static/route_"..i.."_locked.png", _, sc)
			end
		else
			gui.newButton("menu-"..i, _, sW / 2 - 150 * sc, (100 + delta * (i*i)) * sc, 300 * sc, 215 * sc, "graphics/static/route_"..i..".png", _, sc)
		end		
		table.insert(self.menu.buttons, "menu-"..i)
	end
	
	gui.newButton("menu-3", lang.back, sW / 2 - 130 * sc, 720 * sc, 260 * sc, 60 * sc)
	gui.get("menu-3"):setRadiusCorner(gui.get("menu-3").h / 2)
	table.insert(self.menu.buttons, "menu-3")
	
	for i = 1, #self.menu.buttons, 1 do		
		gui.get(self.menu.buttons[i]).fgColor, gui.get(self.menu.buttons[i]).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
--	self.input = "none"
	self.joystickBlocked = false
	
end

function routes:leave()
	
	self.timer:clear()
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("menu-"..i)
	end
	
end

function routes:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function routes:draw()
	
--	TLfres.transform()

	gui.draw()
	if about.settings.gpswitch then
		gui.draw("gamepad")
	end
	
	drawFPS()
	
end

function routes:keypressed(key, unicode)
	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.menu.actions[3]()	end,
		["menu"] = function()	self.menu.actions[3]()	end,
	}
	if switch[key] then switch[key]()	end
--	self.input = key
end

function routes:joystickpressed(joystick, button)

	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.menu.actions[3]()	end
	}
	if switch[button] then switch[button]()	end
--	self.input = button
	
end

return routes