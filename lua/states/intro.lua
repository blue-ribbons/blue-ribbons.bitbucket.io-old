local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local intro = {}

local sW, sH, sc = br.sW, br.sH, br.sc

function intro:init()
	self.timer = timer.new()
end



function intro:enter(state)
	
	about.scene = "intro"
	
	self.logo = love.graphics.newImage("graphics/static/logo_liminalia.png")
	self.target = { opacity = 0 }
	
	self.timer:tween(1, self.target, {opacity = 255}, "linear", function()
		self.timer:tween(1,  self.target, {opacity = 0}, "linear", function()
			if love.filesystem.exists("settings.cfg") then
				local data = love.filesystem.read("settings.cfg")
				local config = json.decode(data)
				about.settings = config.settings
				GS.switch(require("states/mainmenu"))
			else
				GS.switch(require("states/lang"))
			end
		end)
	end)
	
end

function intro:leave()
	self.timer:clear()
end

function intro:update(dt)
	
	self.timer:update(dt)
	
end

function intro:draw()
	
--	TLfres.transform()
	
	love.graphics.setColor({ 255, 255, 255, self.target.opacity })
	love.graphics.draw(self.logo, sW / 2, sH / 2, 0, sc, sc, self.logo:getWidth() / 2, self.logo:getHeight() / 2)
	love.graphics.setColor({ 255, 255, 255, 255 })
	
--	TLfres.letterbox(16, 9)
	
end

return intro