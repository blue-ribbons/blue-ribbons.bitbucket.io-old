local src = require("lang/"..about.settings.lang.."/brm-2")
script = {
	[1] = { "set_db_style", 2 },
	[2] = { "set_page", { name = "BRM2 - Dark Alley", image = "cg-4/33" } },
	[3] = { "add_layer", { name = "chiyo", img = "blue-chiyo2", y = 600 } },
	[4] = { "wait", { time = 0.5 } },
	[5] = { "print", { msg = src[14], mode = 0 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 1 }, { [1] = { text = src[15], name = "brm-2-5" } } },
}
return script