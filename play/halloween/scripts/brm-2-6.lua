local src = require("lang/"..about.settings.lang.."/brm-2")
script = {
	[1] = { "set_db_style", 2 },
	[2] = { "set_page", { name = "BRM2 - Dark Alley", image = "cg-4/34" } },
	[3] = { "play", { mode = 0, id = 26 } },
	[4] = { "set_page", { name = "BRM2 - Dark Alley", image = "cg-4/35" }, { mode = "fade", dir = "right", time = 0.5 } },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "fade_text", { dir = "in", time = 0.5 } },
	[7] = { "clear" },
	[8] = { "set_page", { name = "BRM2 - Costume Show", image = "cg-4/36" }, { mode = "flip", dir = "left", time = 1 } },
	[9] = { "fade_text", { dir = "out", time = 1 } },
	[10] = { "print", { msg = src[19], mode = 0 } },
	[11] = { "wait", { time = 0.5 } },
	[12] = { "input", { type = 1 }, { [1] = { text = src[20], name = "brm-2-7" } } },
	
}
return script