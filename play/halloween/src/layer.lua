--// Layer represents a sprite instance that can be moved across the page

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy = game.cx, game.cy

layer = class {}

function layer:init(ld)
	self.name = ld.name
	self.image = love.graphics.newImage("graphics/sprites/"..ld.img..".png")
	self.x, self.y, self.r, self.sx, self.sy, self.ox, self.oy = ld.x or  sW / 2, ld.y or sH / 2, ld.r or 0, ld.sx or 1, ld.sy or 1, ld.ox or self.image:getWidth() / 2, ld.oy or self.image:getHeight() / 2
	self.color = ld.color or {	r = 255, g = 255, b = 255, alpha = 255	}
	self.timer = timer.new()
	self.isVisible = true
	self.state = 0
end

function layer:update(dt)
	self.timer:update(dt)
end

function layer:draw()
	if self.isVisible then
		love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
		love.graphics.draw(self.image, self.x, self.y, self.r, self.sx * sc, self.sy * sc, self.ox, self.oy)
	end
end

function layer:hide(time, onComplete)
	local t = time or 2
	local oc = onComplete or function()	self.color.alpha = 0	self.state = 0	end
	self.state = 1
	self.timer:tween(t,  self.color, {	alpha = 0	}, "linear", oc)
end

function layer:show(time, onComplete)
	local t = time or 2
	local oc = onComplete or function()	self.color.alpha = 255	self.state = 0	end
	self.state = 1
	self.timer:tween(t,  self.color, {	alpha = 255	}, "linear", oc)
end

function layer:set(lp)
	if lp.x then self.x = cx + (lp.x * sc) end
	if lp.y then self.y = lp.y * sc end
	if lp.r then self.r = lp.r end
	if lp.s then self.sx, self.sy = lp.s, lp.s end
	if lp.color then self.color = lp.color end
	if lp.isVisible then self.isVisible = lp.isVisible end
end