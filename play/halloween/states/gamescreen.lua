require "src/pager"
local timer = require("libs/hump/timer")
local gamescreen = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function gamescreen:init()	
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx	
end

function gamescreen:enter(state, rewind)

	about.scene = "gamescreen"
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.action = function()
		if self.pager and self.pager.input.state == 1 then
			gooi.get("menu_btn").enabled = false
			TEsound.stop("bgm")
			TEsound.play(self.sfx[1], "click", 0.6)
			about.session.cslot.line = self.pager.current_line - 1
			self.pager:destroy()
			self.timer:add(0.01, function()	
					GS.switch(require("states/gameoptions"))	
				end)
		end
	end
	
--	gooi.newButton("menu_btn", _, xc - (64 * sc + 12 * sc), 12 * sc, 64 * sc, 64 * sc, "graphics/static/gear.png", "menu", sc)
--	gooi.get("menu_btn"):onRelease(self.action)
	
	self.joystickBlocked = false
	--// either resume pager from image, restore one from saved script and line pointers or make a completely new one
	if about.session.cslot.script then
		if about.session.cslot.line then
			self.pager = pager()
			print("rewind: "..tostring(rewind))
			if rewind then self.pager:rewind(0, about.session.cslot.script, about.session.cslot.line)
			else		
				self.pager:open(about.session.cslot.script, about.session.cslot.line)
				self.pager:read()
			end
		else
			self.pager = pager()
			self.pager:open(about.session.cslot.script)
			self.pager:read()
		end
	else
	end
	
end

function gamescreen:leave()
	self.timer:clear()
end

function gamescreen:resume()
	self.pager:rewind()
end

function gamescreen:update(dt)
	
	self.pager:update(dt)
	
--	gooi.update(dt)
		
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
		
	if self.joystickBlocked == false then
		
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.pager.input:keypressed("up", unicode, self.pager.input)
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.pager.input:keypressed("down", unicode, self.pager.input)
			end
		end
			
	end
	
	self.timer:update(dt)
	
end

function gamescreen:draw()
	self.pager:draw()
	love.graphics.setColor(255, 255, 255)
	gooi.draw()	
	drawFPS()	
	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle("fill", 0, 0, cx, sH)
	love.graphics.rectangle("fill", xc, 0, sW - xc, sH)
	love.graphics.setColor(255, 255, 255)
end

function gamescreen:leave()
	self.timer:clear()	
	--gooi.remove("menu_btn")
end

function gamescreen:keypressed(key, unicode)
	if key == "escape"	then
		self.action()
	else
		self.pager.input:keypressed(key, unicode, self.pager.input)		--// a tricky way to get a handle of this object
	end
end

function gamescreen:joystickpressed(joystick, button)
	if button == 11 then
		self.action()
	else
		self.pager.input:joystickpressed(joystick, button, self.pager.input)
	end	
end

return gamescreen