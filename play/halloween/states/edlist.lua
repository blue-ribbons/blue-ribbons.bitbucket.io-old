local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local edlist = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function edlist:init()
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx	
end

function edlist:enter(state)
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	about.scene = "edlist"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}				
	if love.filesystem.exists("pager-save") then
		local data = love.filesystem.read("pager-save")
		savefile = json.decode(data)
	end
	
	local src = {
		[1] = { [1] = "graphics/static/ed1.png", [2] = "BRM True End" },
		[2] = { [1] = "graphics/static/ed2.png", [2] = "BRM Rebuild End" },
	}
	
	local images = {
		[1] = "graphics/static/locked.png",
		[2] = "graphics/static/locked.png",
	}
	
	local labels = {
		[1] = lang.locked,
		[2] = lang.locked,
	}
	
	for i = 1, #savefile.achievements.ed, 1 do 
		if savefile.achievements.ed[i] == 1 then
			images[i] = src[i][1]
			labels[i] = src[i][2]
		end
	end
	
	self.menu = {}	

	self.action = function()	
		TEsound.play(self.sfx[1], "click", 0.6)
		GS.switch(require("states/mainmenu"))
	end
	
	gooi.newPanel("edlist-grid", cx + 170 * sc, 100 * sc, (xc - cx) - 340 * sc, sH/3 * 2, "grid 2x2")
	for i = 1, #images, 1 do
		gooi.get("edlist-grid"):add(
			gooi.newButton("edlist-"..i, _, _, _, _, _, images[i], _, sc)
		)
		gooi.get("edlist-"..i).showBorder = false
		gooi.get("edlist-"..i).howRound = 0
		gooi.get("edlist-"..i):generateBorder()
		table.insert(self.menu, "edlist-"..i)
	end	
	for i = 1, #labels, 1 do
		gooi.get("edlist-grid"):add(
			gooi.newLabel("edlist-"..(i + #images), labels[i], _, _, _, _, _, "left")
		)
		gooi.get("edlist-"..i).showBorder = false
		gooi.get("edlist-"..i).howRound = 0
		gooi.get("edlist-"..i):generateBorder()
		table.insert(self.menu, "edlist-"..(i + #images))
	end
	
	gooi.newButton("edlist-"..(#self.menu + 1), lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("edlist-"..(#self.menu + 1)):onRelease(function() self.action() end)
	gooi.get("edlist-"..(#self.menu + 1)).howRound = 1
	gooi.get("edlist-"..(#self.menu + 1)):generateBorder()
	table.insert(self.menu, "edlist-"..(#self.menu + 1))
	
	self.joystickBlocked = false
end

function edlist:leave()	
	self.timer:clear()
	for i = 1, #self.menu, 1 do
		gooi.remove("edlist-"..i)
	end
	gooi.remove("edlist-grid")
end

function edlist:update(dt)	
	gooi.update(dt)
	local function joystickHandler()	
		self.joystickBlocked = true
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end	
	if self.joystickBlocked == false then	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.action()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.action()
			end
		end		
	end	
	self.timer:update(dt)	
end

function edlist:draw()
	gooi.draw()
	drawFPS()
end

function edlist:keypressed(key, unicode)
	self.action()
end

function edlist:joystickpressed(joystick, button)
	self.action()
end
return edlist