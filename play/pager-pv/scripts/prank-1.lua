local src = require("lang/"..about.settings.lang.."/prank")
script = {
	[1] = { "fill", { instant = true, mode = "in" } },
	[2] = { "fade_text", { dir = "in", instant = true } },
	[3] = { "set_page", { name = "Intro", image = "1" } },	
	[4] = { "play", { mode = 1, name = "se221", loop = true } },
	[5] = { "fill", { mode = "out", time = 3 } },
	[6] = { "fade_text", { dir = "out", time = 1 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "input", { type = 1 }, { [1] = { text = src[2], name = "prank-2" }}},
--	[63] = { "redirect", 'main.html' },
}
return script