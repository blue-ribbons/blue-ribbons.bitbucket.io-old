local game = {}	-- a table of objects I'd rather load once than every time the gamestate changes
local mpow, mfloor, sbyte = math.pow, math.floor, string.byte

game.math_round = function( roundIn , roundDig )
	
	if not roundDig then roundDig = 2 end
	
	local mul = mpow( 10, roundDig )
    return ( mfloor( ( roundIn * mul ) + 0.5 ) / mul )

end

game.sW, game.sH = love.graphics.getWidth(), love.graphics.getHeight()
game.sc = game.math_round(game.sH / 720, 3)

game.fonts = {
	["regular"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 32 * game.sc),
	["large"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 64 * game.sc),
	["small"] = love.graphics.newFont("graphics/fonts/FiraSans-ExtraLightItalic.otf", 32 * game.sc)
}

game.gpColor = { fg = { 156, 156, 156 }, bg = { 181, 181, 181, 75 } }
game.uiColor = { fg = { 201, 241, 254 }, bg = { 201, 241, 254, 50 } }
game.activeColor = { 22, 194, 251, 185 }

game.style = {
	bgColor = { 201, 241, 254, 64 },
	fgColor = { 201, 241, 254 },
	howRound = 0.3,
	showBorder = true,
	borderColor = { 201, 241, 254 },
	font = game.fonts["regular"],
	borderProportion = .1,
}

game.background = love.graphics.newImage("graphics/static/background.png")

game.sfx = {}
game.sfx[1] = "sound/sfx/click.ogg"
game.sfx[2] = "sound/sfx/switch.ogg"
game.sfx[3] = "sound/sfx/se107.ogg"
game.sfx[4] = "sound/sfx/se141.ogg"
game.sfx[5] = "sound/sfx/se082.ogg"
game.sfx[6] = "sound/sfx/se077.ogg"
game.sfx[7] = "sound/sfx/se215.ogg"
game.sfx[8] = "sound/sfx/se032.ogg"
game.sfx[9] = "sound/sfx/se122.ogg"
game.sfx[10] = "sound/sfx/se414.ogg"
game.sfx[11] = "sound/sfx/notification.ogg"

game.title = "Pager 1.2"
--[[
if game.sc ~= 1 then
	game.cx, game.cy = game.sW / 2 - 475 * game.sc, 0
	game.xc, game.yc = game.sW / 2 + 475 * game.sc, game.sH
else
	game.cx, game.cy, game.xc, game.yc = 0, 0, 950, 720
end	]]
game.cx, game.cy = game.sW / 2 - 475 * game.sc, 0
game.xc, game.yc = game.sW / 2 + 475 * game.sc, game.sH

game.za = {			--// new coords were manifested because page origin changed from top-left to center
	["top-left"] = { x = 316,6 * game.sc, y = 240 * game.sc },
	["top-center"] = { x = 0, y = 240 * game.sc },
	["top-right"] = { x = -316,6 * game.sc, y = 240 * game.sc },
	["center-left"] = { x = 316,6 * game.sc, y = 0 },
	["center-center"] = { x = 0, y = 0 },
	["center-right"] = { x = -316,6 * game.sc , y = 0 },
	["bottom-left"] = { x = 316,6 * game.sc, y = -240 * game.sc },
	["bottom-center"] = { x = 0, y = -240 * game.sc },
	["bottom-right"] = { x = -316,6 * game.sc, y = -240 * game.sc },
}

if web == true then
	game.fonts["small"] = love.graphics.newFont("graphics/fonts/FiraSans-ExtraLightItalic.otf", 16 * game.sc)
end

game.drawFPS = function()	
	if web == true then
		love.graphics.setFont(game.fonts["small"])
		love.graphics.print("FPS: "..love.timer.getFPS(), game.cx + 8 * game.sc, _, _, 0.5 * game.sc, 0.5 * game.sc)
		love.graphics.print("Pager v"..about.ver.."-web", game.xc - 175 * game.sc, game.yc - 32 * game.sc, _, 0.1 * game.sc, 0.1 * game.sc)
		love.graphics.setFont(game.fonts["regular"])
	else
		love.graphics.print("FPS: "..love.timer.getFPS(), game.cx + 8 * game.sc, _, _, 0.5 * game.sc, 0.5 * game.sc)
		love.graphics.setFont(game.fonts["small"])
		love.graphics.print("Pager v"..about.ver, game.xc - 110 * game.sc, game.yc - 32 * game.sc, _, 0.5 * game.sc, 0.5 * game.sc)
	end
end

return game