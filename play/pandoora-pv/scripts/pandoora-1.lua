local src = require("lang/"..about.settings.lang.."/pandoora")
script = {
	[1] = { "set_page", { name = "PanDOORa - Intro", image = "cg-3/1" } },
	[2] = { "fill", { instant = true, mode = "in" } },
	[3] = { "fade_text", { instant = true, dir = "in" } },
	[4]	= { "play", { mode = 1, name = "pandoora", loop = true } },
	[5] = { "fill", { time = 3, mode = "out" } },
	[6] = { "set_font", 2 },
	[7] = { "fade_text", { dir = "out", time = 2 } },
	[8] = { "wait", { time = 0.2 } },
	[9] = { "print", { msg = src[1], mode = 0 } },
	[10] = { "input", { type = 0 } },
	[11] = { "wait", { time = 0.5 } },
	[12] = { "print", { msg = src[2], mode = 1 } },
	[13] = { "wait", { time = 0.2 } },
	[14] = { "input", { type = 0 } },
	[15] = { "wait", { time = 0.5 } },
	[16] = { "fade_text", { dir = "in", time = 2 } },
	[17] = { "play_cancel" },
	[18] = { "play", { mode = 0, id = 12 } },
	[19] = { "play", { mode = 1, name = "se254", loop = true } },
	[20] = { "set_page", { name = "PanDOORa 1", image = "cg-3/2" }, { mode = "fade", time = 3 }},
	[21] = { "clear" },
	[22] = { "set_db_style", 2 },
	[23] = { "wait", { time = 1 } },
	[24] = { "set_page", { name = "PanDOORa 2", image = "cg-3/3" }, { mode = "fade", time = 1 }},
	[25] = { "add_layer", { name = "hands", img = "hands", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[26] = { "fade_layer", "hands", { dir = "out", time = 1 } },
	[27] = { "set_font", 0 },
	[28] = { "fade_text", { dir = "out", time = 0.5 } },
	[29] = { "print", { msg = src[3], mode = 0 } },
	[30] = { "input", { type = 0 } },
	[31] = { "wait", { time = 0.5 } },
	[32] = { "play", { mode = 0, id = 13 } },
	[33] = { "fade_layer", "hands", { dir = "in", time = 0.5 } },
	[34] = { "remove_layer", "hands" },
	[35] = { "add_layer", { name = "nao-2", img = "nao2", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[36] = { "set_layer", "nao-2", { x = 164 } },
	[37] = { "clear" },
	[38] = { "fade_layer", "nao-2", { dir = "out", time = 1 } },
	[39] = { "print", { msg = src[4], mode = 0 } },
	[40] = { "input", { type = 0 } },
	[41] = { "wait", { time = 0.5 } },
	[42] = { "fade_layer", "nao-2", { dir = "in", time = 1 } },
	[43] = { "set_page", { name = "PanDOORa 3", image = "cg-3/4" }, { mode = "fade", time = 1 }},
	[44] = { "print", { msg = src[5], mode = 0 } },
	[45] = { "input", { type = 0 } },
	[46] = { "wait", { time = 0.5 } },
	[47] = { "set_page", { name = "PanDOORa 2", image = "cg-3/3" }, { mode = "fade", time = 1 }},
	[48] = { "fade_layer", "nao-2", { dir = "out", time = 1 } },
	[49] = { "play", { mode = 1, name = "void", loop = true } },
	[50] = { "print", { msg = src[6], mode = 0 } },
	[51] = { "input", { type = 0 } },
	[52] = { "wait", { time = 0.5 } },
	[53] = { "fade_layer", "nao-2", { dir = "in", time = 0.5 } },
	[54] = { "remove_layer", "nao-2" },
	[55] = { "add_layer", { name = "nao-4", img = "nao4", color = { r = 255, g = 255, b = 255, alpha = 0 }}},
	[56] = { "add_layer", { name = "nao-3", img = "nao3", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[57] = { "set_layer", "nao-3", { x = 212 } },
	[58] = { "fade_layer", "nao-3", { dir = "out", time = 0.5 } },
	[59] = { "print", { msg = src[7], mode = 0 } },
	[60] = { "input", { type = 0 } },
	[61] = { "wait", { time = 0.5 } },
	[62] = { "print", { msg = src[8], mode = 0 } },
	[63] = { "print", { msg = src[9], mode = 1 } },
	[64] = { "input", { type = 0 } },
	[65] = { "wait", { time = 0.5 } },
	[66] = { "print", { msg = src[10], mode = 0 } },
	[67] = { "input", { type = 0 } },
	[68] = { "wait", { time = 0.5 } },
	[69] = { "print", { msg = src[11], mode = 1 } },
	[70] = { "input", { type = 0 } },
	[71] = { "wait", { time = 0.5 } },
	[72] = { "print", { msg = src[12], mode = 0 } },
	[73] = { "input", { type = 0 } },
	[74] = { "wait", { time = 0.5 } },
	[75] = { "print", { msg = src[13], mode = 0 } },
	[76] = { "set_layer", "nao-4", { x = 212, color = { r = 255, g = 255, b = 255, alpha = 255 }}},
	[77] = { "fade_layer", "nao-3", { dir = "in", time = 0.5 } },
	[78] = { "remove_layer", "nao-3" },
	[79] = { "fade_layer", "nao-4", { dir = "out", time = 0.5 } },
	[80] = { "print", { msg = src[14], mode = 1 } },
	[81] = { "input", { type = 0 } },
	[82] = { "wait", { time = 0.5 } },
	[83] = { "print", { msg = src[15], mode = 0 } },
	[84] = { "input", { type = 0 } },
	[85] = { "wait", { time = 0.5 } },
	[86] = { "fade_text", { dir = "in", time = 1 } },
	[87] = { "fade_layer", "nao-4", { dir = "in", time = 1 } },
	[88] = { "remove_layer", "nao-4" },
	[89] = { "add_layer", { name = "nao-1", img = "nao1", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[90] = { "add_layer", { name = "nao-5", img = "nao5", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[91] = { "set_layer", "nao-1", { x = 46, y = 656, s = 1.7 } },
	[92] = { "fade_layer", "nao-1", { dir = "out", time = 0.5 } },
	[93] = { "wait", { time = 0.5 } },
	[94] = { "add_layer", { name = "letter", img = "letter" }},
	[95] = { "play_cancel" },
	[96] = { "play", { mode = 0, id = 14 } },
	[97] = { "wait", { time = 0.1 }},
	[98] = { "set_layer", "nao-5", { x = 46, y = 656, s = 1.7, color = { r = 255, g = 255, b = 255, alpha = 255 } } },
	[99] = { "set_page", { name = "PanDOORa 4", image = "cg-3/1" }},
	[100] = { "play", { mode = 0, id = 15 } },
	[101] = { "clear" },
	[102] = { "set_db_style", 1 },
	[103] = { "fade_text", { dir = "out", time = 2 } },
	[104] = { "print", { msg = src[16], mode = 0 } },
	[105] = { "input", { type = 0 } },
	[106] = { "wait", { time = 0.5 } },
	[107] = { "print", { msg = src[17], mode = 1 } },
	[108] = { "input", { type = 0 } },
	[109] = { "wait", { time = 0.5 } },
	[110] = { "fade_text", { dir = "in", time = 1 } },
	[111] = { "remove_layer", "nao-1" },
	[112] = { "fade_layer", "letter", { dir = "in", time = 0.5 } },
	[113] = { "remove_layer", "letter" },
	[114] = { "play", { mode = 0, id = 16 } },
	[115] = { "fade_layer", "nao-5", { dir = "in", time = 1 } },
	[116] = { "remove_layer", "nao-5" },
	[117] = { "set_page", { name = "PanDOORa 5", image = "cg-3/5" }, { mode = "fade", time = 3 }},
	[118] = { "clear" },
	[119] = { "set_font", 1 },
	[120] = { "fade_text", { dir = "out", time = 1 } },
	[121] = { "print", { msg = src[18], mode = 0 } },
	[122] = { "input", { type = 0 } },
	[123] = { "wait", { time = 0.5 } },
	[124] = { "fade_text", { dir = "in", time = 0.5 } },
	[125] = { "clear" },
	[126] = { "set_font", 0 },
	[127] = { "set_page", { name = "PanDOORa 6", image = "static/blank" }, { mode = "fade", time = 0.5 }},
	[128] = { "play", { mode = 0, id = 17 } },
	[129] = { "play", { mode = 0, id = 18 } },
	[130] = { "set_page", { name = "PanDOORa 6", image = "cg-3/6" }, { mode = "fade", time = 0.5 }},
	[131] = { "fade_text", { dir = "out", time = 0.5 } },
	[132] = { "print", { msg = src[19], mode = 0 } },
	[133] = { "input", { type = 0 } },
	[134] = { "wait", { time = 0.5 } },
	[135] = { "print", { msg = src[20], mode = 1 } },
	[136] = { "input", { type = 0 } },
	[137] = { "wait", { time = 0.5 } },
	[138] = { "print", { msg = src[21], mode = 1 } },
	[139] = { "input", { type = 0 } },
	[140] = { "wait", { time = 0.5 } },
	[141] = { "fade_text", { dir = "in", time = 0.5 } },
	[142] = { "clear" },
	[143] = { "fill", { mode = "in", time = 0.5, r = 0, g = 0, b = 0 } },
	[144] = { "play", { mode = 0, id = 19 } },
	[145] = { "set_page", { name = "PanDOORa 7", image = "cg-3/3" } },
	[146] = { "add_layer", { name = "nao-6", img = "nao6" } },
	[147] = { "set_layer", "nao-6", { x = 128, y = 480, r = 0.25 } },
	[148] = { "fill", { mode = "out", time = 0.5 } },
	[149] = { "fade_text", { dir = "out", time = 0.5 } },
	[150] = { "wait", { time = 0.5 } },
	[151] = { "print", { msg = src[22], mode = 0 } },
	[152] = { "input", { type = 0 } },
	[153] = { "wait", { time = 0.5 } },
	[154] = { "print", { msg = src[23], mode = 1 } },
	[155] = { "input", { type = 0 } },
	[156] = { "wait", { time = 0.5 } },
	[157] = { "fade_text", { dir = "in", time = 0.5 } },
	[158] = { "clear" },
	[159] = { "fill", { mode = "in", time = 0.5, r = 0, g = 0, b = 0 } },
	[160] = { "play", { mode = 0, id = 20 } },
	[161] = { "add_layer", { name = "fist", img = "fist" } },
	[162] = { "set_layer", "nao-6", { x = 186, y = 480, r = -0.2 } },
	[163] = { "set_layer", "fist", { x = 1256, y = 512 } },
	[164] = { "fill", { mode = "out", time = 0.5 } },
	[165] = { "fade_text", { dir = "out", time = 0.3 } },
	[166] = { "wait", { time = 0.3 } },
	[167] = { "print", { msg = src[24], mode = 0 } },
	[168] = { "input", { type = 0 } },
	[169] = { "wait", { time = 0.5 } },
	[170] = { "fade_text", { dir = "in", time = 0.5 } },
	[171] = { "clear" },
	[172] = { "play", { mode = 0, id = 21 } },
	[173] = { "fill", { mode = "in", time = 0.5, r = 0, g = 0, b = 0 } },
	[174] = { "zoom_page", { dir = "in", instant = true, coords = "center-left", amount = 2 } },
	[175] = { "set_layer", "nao-6", { x = 100, y = 512, s = 2 } },
	[176] = { "set_layer", "fist", { x = 980, y = 420, r = -0.3 } },
	[177] = { "fill", { mode = "out", time = 0.5 } },
	[178] = { "fill", { mode = "in", time = 0.8, r = 0, g = 0, b = 0 } },
	[179] = { "remove_layer", "nao-6" },
	[180] = { "remove_layer", "fist" },
	[181] = { "add_layer", { name = "nao-7", img = "nao7" } },
	[182] = { "add_layer", { name = "fist2", img = "fist" } },
	[183] = { "set_layer", "nao-7", { x = 256, y = 512, r = -0.2, s = 2 } },
	[184] = { "set_layer", "fist2", { x = 600, y = 512, r = 0.1 } },
	[185] = { "wait", { time = 0.5 } },
	[186] = { "fill", { mode = "out", instant = true } },
	[187] = { "play", { mode = 0, id = 22 } },
	[188] = { "fill", { mode = "in", time = 3, r = 255, g = 255, b = 255 } },
	[189] = { "set_page", { name = "PanDOORa 8", image = "cg-3/3" } },
	[190] = { "remove_layer", "nao-7" },
	[191] = { "set_layer", "fist2", { x = 950, y = 780, r = 0, s = 0.8 } },
	[192] = { "add_layer", { name = "door", img = "door", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[193] = { "add_layer", { name = "portal", img = "portal" } },
	[194] = { "set_layer", "portal", { x = 184, y = 512 } },
	[195] = { "set_layer", "door", { x = 184, y = 444 } },
	[196] = { "fill", { mode = "out", time = 3, r = 255, g = 255, b = 255 } },
	[197] = { "play", { mode = 0, id = 10 } },
	[198] = { "fade_layer", "door", { dir = "out", time = 4 } },
	[199] = { "fade_layer", "portal", { dir = "in", time = 1 } },
	[200] = { "remove_layer", "portal" },
	[201] = { "wait", { time = 1 } },
	[202] = { "play", { mode = 1, name = "pandoora", loop = false } },
	[203] = { "fill", { mode = "in", time = 5 }},
	[204] = { "remove_layer", "fist2" },
	[205] = { "remove_layer", "door" },
	[206] = { "set_page", { name = "PanDOORa - End", image = "static/blank" } },
	[207] = { "set_font", 2 },
	[208] = { "fill", { instant = true, mode = "out" } },
	[209] = { "fade_text", { dir = "out", time = 0.1 } },
	[210] = { "print", { msg = src[25], mode = 0 } },
	[211] = { "input", { type = 0 } },
	[212] = { "wait", { time = 0.5 } },
	[213] = { "print", { msg = src[26], mode = 1 } },
	[214] = { "wait", { time = 0.2 } },
	[215] = { "input", { type = 0 } },
	[216] = { "wait", { time = 0.5 } },
	[217] = { "fade_text", { dir = "in", time = 2 } },
	[218] = { "wait", { time = 2 } },
	[219] = { "add_layer", { name = "pandoora", img = "pandoora", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[220] = { "fade_layer", "pandoora", { dir = "out", time = 1.5 } },
	[221] = { "wait", { time = 1 } },
	[222] = { "fade_layer", "pandoora", { dir = "in", time = 3 } },
	[223] = { "wait", { time = 0.5 } },
	[224] = { "play_cancel" },
	[225] = { "redirect", "http://blueribbons.moe/main.html" },
	[226] = { "quit" },
}
return script