local src = {}
src[1] = '"___________"\nI know what\'s right.'
src[2] = 'The thing that keeps me alive.\nI can\'t turn my back on the thing that has kept me alive.'
src[3] = 'I can\'t let my conviction waver.\nFor the sake of everyone who could not be saved, I can\'t let any more people share in their fate.'
src[4] = 'There\'s no need to talk about the obvious outcome.\nNozaki Umetarou will turn his mind to steel, and become a superhero.'
return src