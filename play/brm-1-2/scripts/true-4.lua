local src = require("lang/"..about.settings.lang.."/true-4")
script = {
	[1] = { "set_page", { name = "Dark Alley 3", image = "cg-1/3" } },
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3]	= { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[4] = { "set_page", { name = "Dark Alley 4", image = "cg-1/4" }, { mode = "fade", time = 0.5 } } ,
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "fade_text", { dir = "in", time = 1 } },
	[9] = { "zoom_page", { dir = "in", time = 3, amount = 3, coords = "center-right"}},
	[10] = { "zoom_page", { dir = "out", time = 1 }},
	[11] = { "fade_text", { dir = "out", time = 1 } },
	[12] = { "print", { msg = src[2], mode = 1 } },
	[13] = { "wait", { time = 0.5 } },
	[14] = { "input", { type = 1 }, { [1] = { text = src[4], name = "true-5" }, [2] = { text = src[5], name = "true-5" }}},
}
return script