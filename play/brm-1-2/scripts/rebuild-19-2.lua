local src = require("lang/"..about.settings.lang.."/rebuild-14-17")
script = {
	[1] = { "set_page", { name = "Heavy Thoughts 4", image = "cg-2/18-2" }},
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3] = { "set_page", { name = "Heavy Thoughts 3", image = "cg-2/17" }, { mode = "flip", dir = "right", time = 1 } },
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "print", { msg = src[4], mode = 0 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 1 }, { [1] = { text = src[5], name = "rebuild-18-1" }, [2] = { text = src[6], name = "rebuild-18-2"}, [3] = { text = src[7], name = "rebuild-18-3" }, } },
}
return script