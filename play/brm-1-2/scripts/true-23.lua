local src = require("lang/"..about.settings.lang.."/true-23")
script = {
	[1] = { "set_page", { name = "Dark Alley 22", image = "cg-1/22" }} ,
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "set_page", { name = "Dark Alley 23", image = "cg-1/23" }, { mode = "fade", time = 2 }} ,
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "print", { msg = src[1], mode = 0 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-24" } } },
}
return script