local src = require("lang/"..about.settings.lang.."/rebuild-36-37")
script = {
	[1] = { "set_page", { name = "Nisekoi", image = "cg-2/35" }},
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3] = { "set_page", { name = "Nisekoi", image = "cg-2/36" }, { mode = "fade", time = 1 }},
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "set_color" },
	[6] = { "print", { msg = " ", mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 0 }},
	[9] = { "fill", { mode = "in", instant = true } },
	[10] = { "fade_text", { dir = "in", instant = true } },
	[11] = { "set_page", { name = "The End", image = "cg-2/37" }},
	[12] = { "play", { mode = 1, name = "se221", loop = true } },
	[13] = { "unlock", { type = 1, id = 10 } },
	[14] = { "fill", { mode = "out", instant = true } },
	[15] = { "fade_text", { dir = "out", time = 1 } },
	[16] = { "print", { msg = src[1], mode = 0 } },
	[17] = { "wait", { time = 0.5 } },
	[18] = { "input", { type = 1 }, { [1] = { text = src[2], name = "rebuild-38-40" } } },
}	
return script