local src = require("lang/"..about.settings.lang.."/true-25")
script = {
	[1] = { "set_page", { name = "Dark Alley 24", image = "cg-1/24" }} ,
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "set_page", { name = "Dark Alley 22", image = "cg-1/25" }, { mode = "fade", time = 1 }} ,
	[4] = { "unlock", { type = 1, id = 1 } },
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 0 } } ,
	[9] = { "fade_text", { dir = "in", time = 0.5 } },
	[10] = { "clear" },
	[11] = { "fill", { mode = "in", time = 3 }},
	[12] = { "play_cancel" },
	[13] = { "read", { name = "true-26" } },
}
return script