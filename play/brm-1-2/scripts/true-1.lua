local src = require("lang/"..about.settings.lang.."/true-1")
script = {
	[1] = { "set_page", { name = "Dark Alley 1", image = "cg-1/1" } },
	[2] = { "fill", { instant = true, mode = "in" } },
	[3] = { "fade_all", { dir = "in", time = 0.001 } },
	[4]	= { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[5] = { "wait", { time = 1 } },
	[6] = { "fill", { instant = true, mode = "out" } },
	[7] = { "fade_page", { dir = "out", time = 4 } },
	[8] = { "fade_text", { dir = "out", time = 1 } },
	[9] = { "unlock", { type = 2, id = 1 } },
	[10] = { "print", { msg = src[1], mode = 0 } },
	[11] = { "wait", { time = 0.5 } },
	[12] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-2-1" }, [2] = { text = src[3], name = "true-2-2" } } },
}
return script