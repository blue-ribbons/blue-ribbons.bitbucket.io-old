local src = require("lang/"..about.settings.lang.."/true-46")
script = {
	[1] = { "set_page", { name = "Dark Alley [End]", image = "cg-1/46" }},
	[2] = { "play", { mode = 0, id = 8 } },
	[3] = { "play_cancel" },
	[4] = { "wait", { time = 2 } },
	[5] = { "play", { mode = 1, name = "se221", loop = true } },
	[6] = { "fill", { time = 3, mode = "out" } },
	[7] = { "wait", { time = 2 } },
	[8] = { "fade_page", { time = 3, mode = "in" } },
	[9] = { "fade_text", { dir = "out", time = 0.5 } },
	[10] = { "print", { msg = src[1], mode = 0 } },
	[11] = { "wait", { time = 3 } },
	[12] = { "fade_text", { dir = "in", time = 3 } },
	[13] = { "clear" },	
	[14] = { "play_cancel" },
	[15] = { "unlock", { type = 3, id = 1 } },
	[16] = { "finish", 1 }
}
return script