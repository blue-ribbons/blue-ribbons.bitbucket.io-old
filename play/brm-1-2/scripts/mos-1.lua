local src = require("lang/"..about.settings.lang.."/rebuild-19-2")
script = {
	[1] = { "set_page", { name = "Heavy Thoughts 4", image = "cg-2/18-2" }},
	[2] = { "play", { mode = 1, name = "silver_moon", loop = 1 } },
	[3] = { "fade_text", { dir = "in", time = 1 } },
	[4] = { "unlock", { type = 2, id = 8 } },
	[5] = { "fade_page", { dir = "in", time = 2 } },
	[6] = { "fade_text", { dir = "out", time = 0.5 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "input", { type = 0 }},
	[9] = { "fade_text", { dir = "in", time = 0.5 } },
	[10] = { "clear" },
	[11] = { "fill", { mode = "in", instant = true}},
	[12] = { "set_page", { name = "Mind of Steel", image = "cg-2/23" } },
	[13] = { "fade_page", { dir = "in", time = 0.1 } },
	[14] = { "fill", { mode = "out", instant = true}},
	[15] = { "fade_page", { dir = "out", time = 1 } },
	[16] = { "wait", { time = 0.5 } },
	[17] = { "fade_page", { dir = "in", time = 1 } },
	[18] = { "fade_text", { dir = "out", time = 0.5 } },
	[19] = { "print", { msg = src[2], mode = 0 } },
	[20] = { "input", { type = 0 }},
	[21] = { "print", { msg = src[3], mode = 0 } },
	[22] = { "input", { type = 0 }},
	[23] = { "print", { msg = src[4], mode = 0 } },
	[24] = { "input", { type = 0 }},
	[25] = { "fade_text", { dir = "in", time = 2 } },
	[26] = { "set_align", "center" },
	[27] = { "print", { msg = "\n\n\n\n\n\nBAD END", mode = 0 } },
	[28] = { "fade_text", { dir = "out", time = 3 } },
	[29] = { "fill", { mode = "in", time = 5 }},
	[30] = { "play_cancel" },
	[31] = { "play", { mode = 1, name = "01_faint_voice", loop = true } },
	[32] = { "exec", "src/bad_end" }
}
return script