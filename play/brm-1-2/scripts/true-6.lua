local src = require("lang/"..about.settings.lang.."/true-6")
script = {
	[1] = { "set_page", { name = "Dark Alley 5", image = "cg-1/5" } },
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3] = { "set_page", { name = "Dark Alley 6", image = "cg-1/6" }, { mode = "fade", time = 1 } } ,
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "print", { msg = src[1], mode = 0 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-7" }, [2] = { text = src[3], name = "true-7" }, [3] = { text = src[4], name = "true-7" }}},
}
return script