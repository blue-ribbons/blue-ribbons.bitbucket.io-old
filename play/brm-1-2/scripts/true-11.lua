local src = require("lang/"..about.settings.lang.."/true-11")
script = {
	[1] = { "fill", { mode = "in", r = 255, g = 255, b = 255, instant = true } },
	[2] = { "set_page", { name = "Dark Alley 11", image = "cg-1/11" } },
	[3] = { "play", { mode = 0, id = 4 } },
	[4] = { "play_cancel" },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "fill", { mode = "out", r = 255, g = 255, b = 255, time = 0.5 } },
	[7] = { "clear" },
	[8] = { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[9] = { "fade_text", { dir = "out", time = 2 } },
	[10] = { "print", { msg = src[1], mode = 0 } },
	[11] = { "input", { type = 0 }},
	[12] = { "print", { msg = src[2], mode = 1 } },
	[13] = { "input", { type = 0 }},
	[14] = { "read", { name = "true-12" } },
}
return script