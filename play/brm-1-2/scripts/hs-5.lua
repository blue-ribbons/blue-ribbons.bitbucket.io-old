local src = require("lang/"..about.settings.lang.."/hs")
script = {
	[1] = { "set_db_style", 2 },
	[2] = { "set_page", { name = "Halloween - Dark Alley", image = "cg-4/33" } },
	[3] = { "add_layer", { name = "chiyo", img = "blue-chiyo", y = 600 } },
	[4] = { "wait", { time = 0.5 } },
	[5] = { "print", { msg = src[16], mode = 0 } },
	[6] = { "input", { type = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "fade_text", { dir = "in", time = 0.5 } },
	[9] = { "clear" },
	[10] = { "fade_layer", "chiyo", { dir = "in", time = 0.5 } },
	[11] = { "play", { mode = 1, name = "gsnk08", loop = true } },
	[12] = { "set_page", { name = "Halloween - Costume Show", image = "cg-4/34" }, { mode = "flip", dir = "right", time = 1 } },
	[13] = { "unlock", { type = 1, id = 11 } },
	[14] = { "fade_text", { dir = "out", time = 1 } },
	[15] = { "print", { msg = src[17], mode = 0 } },
	[16] = { "wait", { time = 0.5 } },
	[17] = { "input", { type = 1 }, { [1] = { text = src[18], name = "hs-6" } } },	
}
return script