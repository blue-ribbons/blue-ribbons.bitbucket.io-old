local src = require("lang/"..about.settings.lang.."/true-9")
script = {
	[1] = { "set_page", { name = "Dark Alley 8", image = "cg-1/8" } },
	[2] = { "print", { msg = src[1], mode = 0 } },
	[3] = { "wait", { time = 0.5 } },
	[4] = { "set_page", { name = "Dark Alley 9", image = "cg-1/9" }, { mode = "fade", time = 1 } } ,
	[5] = { "print", { msg = src[2], mode = 1 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 0 }},
	[8] = { "read", { name = "true-10" } },
}
return script