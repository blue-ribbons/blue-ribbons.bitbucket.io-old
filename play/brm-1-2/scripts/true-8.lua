local src = require("lang/"..about.settings.lang.."/true-8")
script = {
	[1] = { "set_page", { name = "Dark Alley 7", image = "cg-1/7" } },
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3]	= { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[4] = { "set_page", { name = "Dark Alley 8", image = "cg-1/8" }, { mode = "fade", time = 0.5 } } ,
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-9" }}},
}
return script