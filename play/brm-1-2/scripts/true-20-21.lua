local src = require("lang/"..about.settings.lang.."/true-20-21")
script = {
	[1] = { "set_page", { name = "Dark Alley 20", image = "cg-1/20" }} ,
	[2] = { "fill", { mode = "out", time = 3 } },
	[3] = { "fade_text", { dir = "out", time = 1 } },
	[4] = { "print", { msg = src[1], mode = 0 } },
	[5] = { "input", { type = 0 }},
	[6] = { "fade_text", { dir = "in", time = 0.5 } },
	[7] = { "clear" },
	[8] = { "set_page", { name = "Dark Alley 21", image = "cg-1/21" }, { mode = "fade", time = 1 }} ,
	[9] = { "fade_text", { dir = "out", time = 1 } },
	[10] = { "print", { msg = src[2], mode = 0 } },
	[11] = { "input", { type = 0 }},
	[12] = { "fade_text", { dir = "in", time = 0.5 } },
	[13] = { "fill", { mode = "in", time = 3 } },
	[14] = { "clear" },
	[15] = { "read", { name = "true-22" } },
}
return script