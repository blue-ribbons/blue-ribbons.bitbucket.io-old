var started = false;

jQuery(function($, undefined) {
  $('#prompt').terminal(function(cmd, term) {
    if (cmd == 'help') {
      term.echo(String(
      "ls               show files in directory\n" +
      "cat    [FILE]    output contents of a file to standard output\n" +
      "start            start our quiz!\n" +
      "help             show this prompt"
      ));
    } else if (cmd == 'ls') {
      term.echo(String("[[;green;]README.txt]"));
    } else if (cmd == 'cat') {
      term.echo(String("[[;red;]Error] - missing argument. Usage: [[;white;]cat [FILE]]"));
    } else if (cmd == 'cat README.txt') {
      term.echo(String(
  " _______  ___      __   __  _______  ______    ___   _______  _______  _______  __    _  _______        _______  ___  \n" +
  "|  _    ||   |    |  | |  ||       ||    _ |  |   | |  _    ||  _    ||       ||  |  | ||       |      |   _   ||   | \n" +
  "| |_|   ||   |    |  | |  ||    ___||   | ||  |   | | |_|   || |_|   ||   _   ||   |_| ||  _____|      |  |_|  ||   | \n" +
  "|       ||   |    |  |_|  ||   |___ |   |_||_ |   | |       ||       ||  | |  ||       || |_____       |       ||   | \n" +
  "|  _   | |   |___ |       ||    ___||    __  ||   | |  _   | |  _   | |  |_|  ||  _    ||_____  | ___  |       ||   | \n" +
  "| |_|   ||       ||       ||   |___ |   |  | ||   | | |_|   || |_|   ||       || | |   | _____| ||   | |   _   ||   | \n" +
  "|_______||_______||_______||_______||___|  |_||___| |_______||_______||_______||_|  |__||_______||___| |__| |__||___| \n" +
      "\n" +
      "Version: 0.1 alpha\n" + 
      "\n" +
      "Description: lorem ipsum blablabla\n" +
      "\n" +
      "SAE Works, 2016\n"
      ));
    } else if (cmd == 'start') {
      started = true;
    }
  }, {
    greetings: '[[;blue;]BlueRibbons.ai] welcomes you! Type [[;white;]\'help\'] for available commands.',
    name: 'js_demo',
    prompt: '[[;blue;]BlueRibbons.ai>] ',
    width: '99%',
    height: '100%'
    });
});